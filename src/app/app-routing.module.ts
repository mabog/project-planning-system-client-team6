import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { RouterModule, Routes, PreloadAllModules } from '@angular/router';
import { LoginComponent } from './components/login/login.component';
import {
  ProjectsComponent
} from './modules/projects/components/projects.component';
import {
  AllProjectsComponent
} from './modules/projects/components/all-projects/all-projects.component';
import {
  MyManagedProjectsComponent
} from './modules/projects/components/my-managed-projects/my-managed-projects.component';
import {
  CreateProjectComponent
} from './modules/projects/components/create-project/create-project.component';
import {
  ProjectDetailsComponent
} from './modules/projects/components/project-details/project-details.component';

const routes: Routes = [
  { path: '', redirectTo: 'login', pathMatch: 'full' },
  { path: 'login', component: LoginComponent },
  {
    path: 'dashboard',
    loadChildren: () => import('./modules/dashboard/dashboard.module')
      .then(m => m.DashboardModule)
  },
  {
    path: 'work-force',
    loadChildren: () => import('./modules/work-force/work-force.module')
      .then(m => m.WorkForceModule)
  },
  { path: 'projects', component: ProjectsComponent },
  { path: 'all-projects', component: AllProjectsComponent },
  { path: 'my-managed-projects', component: MyManagedProjectsComponent },
  { path: 'create-project', component: CreateProjectComponent },
  { path: 'project-details/:id', component: ProjectDetailsComponent },
  { path: 'edit-project/:id', component: CreateProjectComponent },

];


@NgModule({
  declarations: [],
  imports: [
    CommonModule,
    RouterModule.forRoot(routes, {
      preloadingStrategy: PreloadAllModules,
      onSameUrlNavigation: 'reload',
    })
  ],
  exports: [RouterModule]
})
export class AppRoutingModule { }

