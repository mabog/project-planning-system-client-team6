import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';
import { AppRoutingModule } from './app-routing.module';
import { AppComponent } from './app.component';
import { LoginComponent } from './components/login/login.component';
import { RouterModule } from '@angular/router';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { JwtModule } from '@auth0/angular-jwt';
import { HttpClientModule, HTTP_INTERCEPTORS } from '@angular/common/http';
import { NavbarComponent } from './components/navbar/navbar.component';
import {
  NoopAnimationsModule, BrowserAnimationsModule
} from '@angular/platform-browser/animations';
import { LayoutModule } from '@angular/cdk/layout';
import { NgxSpinnerModule } from 'ngx-spinner';
import {
  SpinnerIntercerptorService
} from './middleware/interceptors/spinner-interceptor.service';
import {
  TokenInterceptorService
} from './auth/token-interceptor.service';
import { CoreModule } from './core/core.module';
import { SharedModule } from './shared/shared.module';
import { AuthGuard } from './auth/auth.guard';
import { MaterialModule } from './material/material.module';
import {
  ProjectsComponent
} from './modules/projects/components/projects.component';
import {
  AllProjectsComponent
} from './modules/projects/components/all-projects/all-projects.component';
import {
  MyManagedProjectsComponent
} from './modules/projects/components/my-managed-projects/my-managed-projects.component';
import {
  ProjectComponent
} from './modules/projects/components/project/project.component';
import {
  ProjectDetailsComponent
} from './modules/projects/components/project-details/project-details.component';
import {
  CreateProjectComponent
} from './modules/projects/components/create-project/create-project.component';


@NgModule({
  declarations: [
    AppComponent,
    LoginComponent,
    NavbarComponent,
    ProjectsComponent,
    AllProjectsComponent,
    MyManagedProjectsComponent,
    ProjectComponent,
    ProjectDetailsComponent,
    CreateProjectComponent,
  ],
  imports: [
    BrowserModule,
    BrowserAnimationsModule,
    CoreModule,
    SharedModule,
    MaterialModule,
    AppRoutingModule,
    HttpClientModule,
    RouterModule,
    FormsModule,
    ReactiveFormsModule,
    JwtModule.forRoot({
      config: {
        tokenGetter: () => (localStorage.getItem('accessToken')),
        whitelistedDomains: ['localhost:4200']
      }
    }),
    NoopAnimationsModule,
    LayoutModule,
    NgxSpinnerModule,
  ],
  providers: [
    AuthGuard,
    {
      provide: HTTP_INTERCEPTORS,
      useClass: SpinnerIntercerptorService,
      multi: true,
    },
    {
      provide: HTTP_INTERCEPTORS,
      useClass: TokenInterceptorService,
      multi: true,
    },
  ],
  bootstrap: [AppComponent]
})
export class AppModule { }
