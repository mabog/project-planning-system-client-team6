import { Injectable } from '@angular/core';
import {
  HttpRequest,
  HttpHandler,
  HttpEvent,
  HttpInterceptor,
  HttpResponse
} from '@angular/common/http';
import { Observable } from 'rxjs';

import { map } from 'rxjs/operators';
import { AuthService } from '../core/services/auth.service';
import { StorageService } from '../core/services/storage.service';

@Injectable()
export class TokenInterceptorService implements HttpInterceptor {

  constructor(
    private readonly auth: AuthService,
    private readonly storage: StorageService) { }

  intercept(
    request: HttpRequest<any>,
    next: HttpHandler
  ): Observable<HttpEvent<any>> {
    this.auth.isLoading$.next(true);
    request = request.clone({
      setHeaders: {
        Authorization: `Bearer ${this.storage.getItem('accessToken')}`
      }
    });

    return next.handle(request).pipe(
      map((event: HttpEvent<any>) => {
        if (event instanceof HttpResponse) {
          this.auth.isLoading$.next(false);
        }
        return event;
      }));
  }
}
