import { Component } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { Router } from '@angular/router';
import { Validators, FormGroup, FormControl } from '@angular/forms';
import { AuthService } from '../../core/services/auth.service';
import { StorageService } from '../../core/services/storage.service';
import { NotificatorService } from '../../core/services/notificator.service';

@Component({
  selector: 'app-login',
  templateUrl: './login.component.html',
  styleUrls: ['./login.component.css']
})

export class LoginComponent {
  public error = null;
  public loginForm: FormGroup = new FormGroup({
    email: new FormControl('', [Validators.required, Validators.minLength(5)]),
    password: new FormControl('', [Validators.required, Validators.minLength(5)])
  });

  constructor(
    private readonly http: HttpClient,
    private readonly auth: AuthService,
    private readonly storage: StorageService,
    private readonly router: Router,
    private readonly notificator: NotificatorService,
  ) { }

  // for admin
  public register(username: string, password: string): void {
    // this.auth.registerUser(username, password).subscribe(console.log);
  }

  public login(email: string, password: string): void {
    this.auth.loginUser(email, password).subscribe((response) => {
      this.storage.setItem('accessToken', response.accessToken);

      const user = this.auth.getUserFromToken(response.accessToken);
      this.auth.loggedInUser$.next(user);

      this.notificator.success('Login successful!');
      this.router.navigate(['dashboard']);
      this.error = null;
    },
      (error) => {
        this.error = error;
        console.log(error);
        this.notificator.error('Invalid username/password!');
      },
    );
  }


}
