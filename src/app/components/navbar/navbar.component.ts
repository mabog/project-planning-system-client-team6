import { Component, OnInit, OnDestroy } from '@angular/core';
import { Subscription } from 'rxjs';
import { Router } from '@angular/router';
import { AuthService } from '../../core/services/auth.service';

@Component({
  selector: 'app-navbar',
  templateUrl: './navbar.component.html',
  styleUrls: ['./navbar.component.css']
})
export class NavbarComponent implements OnInit, OnDestroy {

  public loggedUser: any; // change
  private subscription: Subscription;

  constructor(
    private readonly router: Router,
    public readonly auth: AuthService,
    ) {  }


  public gotoLogout() {
    this.auth.logoutUser('accessToken');
    this.router.navigate(['/login']);
  }

  public Menu(){}

  ngOnInit() {
    this.subscription = this.auth.loggedInUser$.subscribe((loggedUser) => {
      this.loggedUser = loggedUser;
    });
  }

  ngOnDestroy(): void {
    this.subscription.unsubscribe();
  }
}
