import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { Observable } from 'rxjs';
import { CONFIG } from '../../configs/config';
import {
  AssignmentCreateDTO
} from '../../models/assignments/assignment-create.dto';
import {
  AssignmentReturnDTO
} from '../../models/assignments/assignment-return.dto';

@Injectable({
  providedIn: 'root'
})
export class AssignmentService {

  constructor(
    private http: HttpClient,
  ) { }

  public getAssignments(reqID: string): Observable<AssignmentReturnDTO[]> {
    return this.http.get<AssignmentReturnDTO[]>(`${CONFIG.SERVER_ADDR}/requirements/${reqID}/assignments`);
  }
    public assignWork(employeeID: string, body: AssignmentCreateDTO): Observable<AssignmentReturnDTO> {
    return this.http.post<AssignmentReturnDTO>(`${CONFIG.SERVER_ADDR}/employees/${employeeID}/assignments`, body);
  }

  public getEmployeeAssignments(
    employeeID: string
  ): Observable<AssignmentReturnDTO[]> {

    return this.http.get<AssignmentReturnDTO[]>(
      `${CONFIG.SERVER_ADDR}/employees/${employeeID}/assignments`
    );
  }
}
