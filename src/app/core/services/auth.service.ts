import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { Observable, BehaviorSubject } from 'rxjs';
import { StorageService } from './storage.service';
import { JwtHelperService } from '@auth0/angular-jwt';
import { CONFIG } from '../../configs/config';
import { UserDTO } from '../../models/users/user.dto';

@Injectable({ providedIn: 'root' })
export class AuthService {
  public isLoading$ = new BehaviorSubject<boolean>(false);
  public loggedInUser$ = new BehaviorSubject<{ id: string, name: string }>(
    this.getUserFromToken(this.storage.getItem('accessToken'))
  );

  private readonly isLoggedInSubject$ = new BehaviorSubject<boolean>(
    this.isUserLoggedIn()
  );

  private readonly loggedUserSubject$ = new BehaviorSubject<UserDTO>(
    this.loggedUser()
  );

  constructor(
    private readonly http: HttpClient,
    private readonly storage: StorageService,
    private readonly jwtHelper: JwtHelperService
  ) { }

  public get isLoggedIn$(): Observable<boolean> {
    return this.isLoggedInSubject$.asObservable();
  }

  public get loggedUser$(): Observable<UserDTO> {
    return this.loggedUserSubject$.asObservable();
  }

  public registerUser(name: string, password: string): Observable<any> {

    return this.http.post(
      `${CONFIG.SERVER_ADDR}/users`, { name, password, avatar: 'avatar' }
    );
  }

  public loginUser(email: string, password: string): Observable<any> {

    return this.http.post(
      `${CONFIG.SERVER_ADDR}/session/login`, { email, password }
    );
  }

  public getUserFromToken(accessToken: string) {

    return this.jwtHelper.decodeToken(accessToken);
  }

  public logoutUser(accessToken: string): Observable<any> {
    const options = {
      headers: { /* your headers */ },
      body: { accessToken }
    };
    this.storage.removeItem('accessToken');
    this.loggedInUser$.next(null);

    return this.http.delete(`${CONFIG.SERVER_ADDR}/session`, options);
  }

  private isUserLoggedIn(): boolean {
    return !!this.storage.getItem('accessToken');
  }

  private loggedUser(): UserDTO {
    try {
      return this.jwtHelper.decodeToken(this.storage.getItem('accessToken'));
    } catch (error) {
      // in case of storage tampering
      this.isLoggedInSubject$.next(false);

      return null;
    }
  }
}
