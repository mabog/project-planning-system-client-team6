import { EmployeeReturnDTO } from './../../models/projects/emploee-return.dto';
import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { Observable } from 'rxjs';
import { EmployeeDTO } from '../../models/employees/employee.dto';
import { CONFIG } from '../../configs/config';

@Injectable({
  providedIn: 'root'
})
export class EmployeeService {

  constructor(
    private readonly http: HttpClient
  ) { }

  public getEmployee(employeeID: string): Observable<EmployeeReturnDTO> {
    return this.http.get<EmployeeReturnDTO>(`${CONFIG.SERVER_ADDR}/employees/${employeeID}`);
  }


  public getAllEmployees(): Observable<EmployeeDTO[]> {

    return this.http.get<EmployeeDTO[]>(`${CONFIG.SERVER_ADDR}/employees`);
  }

  public getSingleEmployee(employeeId: string): Observable<EmployeeDTO> {

    return this.http.get<EmployeeDTO>(
      `${CONFIG.SERVER_ADDR}/employees/${employeeId}`
    );
  }


}
