import { ProjectReturnDTO } from './../../models/projects/project-return.dto';
import { CONFIG } from '../../configs/config';
import { Injectable } from '@angular/core';
import { HttpClient, HttpHeaders } from '@angular/common/http';
import { StorageService } from './storage.service';
import { Observable } from 'rxjs';
import { ProjectCreateDTO } from '../../models/projects/project-create.dto';

@Injectable({
  providedIn: 'root'
})
export class ProjectService {

  constructor(
    private http: HttpClient,
    private readonly storage: StorageService
  ) { }

  public getAllProjects(): Observable<any> {
    const headers = new HttpHeaders(
      {Authorization: `Bearer ${this.storage.getItem('accessToken')}`});
    const result = this.http.get(`${CONFIG.SERVER_ADDR}/projects`, {headers});

    return result;
  }

  public getProjectByID(ID: string): Observable<any> {
    const headers = new HttpHeaders(
      {Authorization: `Bearer ${this.storage.getItem('accessToken')}`});
    const result = this.http.get(`${CONFIG.SERVER_ADDR}/projects/${ID}`, {headers});
    return result;
  }

  public getProjectsByManagerID(ID: string): Observable<any> {
    const headers = new HttpHeaders(
      {Authorization: `Bearer ${this.storage.getItem('accessToken')}`});
    const result = this.http.get(
      `${CONFIG.SERVER_ADDR}/projects/${ID}/requirements`, {headers});
    return result;
  }

  public createProject(body: ProjectCreateDTO): Observable<any> {
    return this.http.post<ProjectCreateDTO>(`${CONFIG.SERVER_ADDR}/projects/create`, body);
  }

  public updateProject(id: string, body: ProjectCreateDTO): Observable<ProjectReturnDTO> {
    return this.http.patch<ProjectReturnDTO>(`${CONFIG.SERVER_ADDR}/projects/${id}`, body);
  }

  public getAllEmployyes(): Observable<any> {
    const headers = new HttpHeaders(
      {
        Authorization: `Bearer ${this.storage.getItem('accessToken')}`
      });
    const result = this.http.get(`${CONFIG.SERVER_ADDR}/employees`, {headers});
    return result;
  }


  public deleteProject(projectID: string) {
    return this.http.delete(`${CONFIG.SERVER_ADDR}/projects/${projectID}`);
  }

}
