import { RequirementCreateDTO } from './../../models/projects/requirement-create.dto';
import { Injectable } from '@angular/core';
import { HttpClient, HttpHeaders } from '@angular/common/http';
import { StorageService } from './storage.service';
import { CONFIG } from '../../configs/config';
import { Observable } from 'rxjs/internal/Observable';
import { RequirementReturnDTO } from '../../models/projects/requirement-return.dto';

@Injectable({
  providedIn: 'root'
})
export class RequirementService {

  constructor(
    private http: HttpClient,
    private readonly storage: StorageService
  ) { }

  public getRequirements(projectID: string): Observable<RequirementReturnDTO[]> {
    return this.http.get<RequirementReturnDTO[]>(`${CONFIG.SERVER_ADDR}/projects/${projectID}/requirements`);
  }

  public createRequirement(projectID: string, body: RequirementCreateDTO): Observable<RequirementReturnDTO> {
    return this.http.post<RequirementReturnDTO>(`${CONFIG.SERVER_ADDR}/projects/${projectID}/requirements`, body);
  }

}
