import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { Observable } from 'rxjs';
import { UserDTO } from '../../models/users/user.dto';
import { CONFIG } from '../../configs/config';

import { EmployeeDTO } from '../../models/employees/employee.dto';
import { ProjectDTO } from '../../models/projects/project.dto';

@Injectable({
  providedIn: 'root'
})
export class UserService {

  constructor(
    private readonly http: HttpClient
  ) { }

  public getAllUsers(): Observable<UserDTO[]> {

    return this.http.get<UserDTO[]>(`${CONFIG.SERVER_ADDR}/users`);
  }

  public getMyInfo(): Observable<UserDTO> {

    return this.http.get<UserDTO>(`${CONFIG.SERVER_ADDR}/users/me`);
  }

  public getSingleUser(id: string): Observable<UserDTO> {

    return this.http.get<UserDTO>(`${CONFIG.SERVER_ADDR}/users/${id}`);
  }

  public getUserSubordinates(managerId: string): Observable<UserDTO[]> {

    return this.http.get<UserDTO[]>(
      `${CONFIG.SERVER_ADDR}/users/${managerId}/users`
    );
  }

  public getEmployeeSubordinates(managerId: string): Observable<EmployeeDTO[]> {

    return this.http.get<EmployeeDTO[]>(
      `${CONFIG.SERVER_ADDR}/users/${managerId}/employees`
    );
  }

  public getProjectsForManager(userId: string): Observable<ProjectDTO[]> {

    return this.http.get<ProjectDTO[]>(
      `${CONFIG.SERVER_ADDR}/users/${userId}/projects`
    );
  }
}
