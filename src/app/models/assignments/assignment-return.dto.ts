export class AssignmentReturnDTO {

  public id: string;


  public assignmentSkill: string;


  public employeeWorkInputPerDay: number;


  public totalTimeWorked: number;


  public projectId: string;


  public requirementId: string;


  public assigneeId: string;


  public assignerId: string;


  public isCompleted: boolean;


  public dateAssigned: string;


  public lastUpdated: string;
}
