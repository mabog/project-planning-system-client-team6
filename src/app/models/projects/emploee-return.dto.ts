export class EmployeeReturnDTO {
    public id: string;
    public firstName: string;
    public lastName: string;
    public email: string;
    public skills: string[];
    public totalAvailableWorkTime: number;
    public dateCreated: string;
    public lastUpdated: string;
}
