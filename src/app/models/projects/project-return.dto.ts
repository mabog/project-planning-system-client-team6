export class ProjectReturnDTO {

    public id: string;

    public projectCreator: string;

    public name: string;

    public description: string;

    public status: string;

    public targetInDays: number;

    public managerWorkInputPerDay: number;

    public daysUntilCompletion: number;

    public estCompletionDate: string;

    public completionDate: string;

    public isWithinTarget: boolean;

    public dateCreated: string;

    public lastUpdated: string;
}
