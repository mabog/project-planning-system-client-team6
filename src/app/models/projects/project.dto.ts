export class ProjectDTO {
  public id: string;

  public name: string;

  public description: string;

  public status: string;

  public projectCreator: string;

  public currentManagerId: string;

  public targetInDays: number;

  public isWithinTarget: boolean;

  public managerWorkInputPerDay: number;

  public daysUntilCompletion: number;

  public estCompletionDate: string;

  public isCompleted: boolean;

  public dateCreated: Date;

  public lastUpdated: Date;
}