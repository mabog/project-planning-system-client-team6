export class RequirementReturnDTO {
    public id: string;
    public reqSkill: string; // yes
    public totalReqWorkTime: number; // yes
    public leftReqWorkTime: number; // yes
    public totalCompletedWorkTime: number; // yes
    public completedWorkTimePerDay: number;
    public daysUntilCompletion: number;
    public estCompletionDate: string;
    public isCompleted: boolean;
    public dateCreated: string;
    public lastUpdated: string;
}
