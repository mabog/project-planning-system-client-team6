export class UserDTO {
  public id: string;

  public firstName: string;

  public lastName: string;

  public email: string;

  public totalAvailableWorkTime: number;

  public isAdmin: boolean;

  public isSelfManaged: boolean;

  public managerId: string;

  public positions: string[];

  public skills: string[];

  public dateCreated: string;

  public lastUpdated: string;

  public hasAccess: boolean;

  public avatarUrl: string;
}
