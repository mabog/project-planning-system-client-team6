import { Component, OnInit } from '@angular/core';
import { UserService } from '../../../core/services/user.service';
import { UserDTO } from '../../../models/users/user.dto';

@Component({
  selector: 'app-board',
  templateUrl: './board.component.html',
  styleUrls: ['./board.component.css']
})
export class BoardComponent implements OnInit {
  public userInfo: UserDTO;
  public directManager: UserDTO;

  constructor(
    private readonly userService: UserService,
  ) { }

  ngOnInit() {
    this.userService.getMyInfo()
      .subscribe({
        next: (data: UserDTO) => {
          this.userInfo = data;

          if (!this.userInfo.isSelfManaged) {
            this.userService.getSingleUser(this.userInfo.managerId)
              .subscribe({
                next: (manager: UserDTO) => {
                  this.directManager = manager;
                }
              });
          }
        },
        error: e => console.log(e),
      });
  }
}
