import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { ManagedProjectsDetailsComponent } from './managed-projects-details.component';

describe('ManagedProjectsDetailsComponent', () => {
  let component: ManagedProjectsDetailsComponent;
  let fixture: ComponentFixture<ManagedProjectsDetailsComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ ManagedProjectsDetailsComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(ManagedProjectsDetailsComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
