import { Component, OnInit, Input } from '@angular/core';
import { ProjectDTO } from '../../../../../models/projects/project.dto';
import { UserService } from '../../../../../core/services/user.service';
import { UserDTO } from '../../../../../models/users/user.dto';
import { Router } from '@angular/router';

@Component({
  selector: 'app-managed-projects-details',
  templateUrl: './managed-projects-details.component.html',
  styleUrls: ['./managed-projects-details.component.css']
})
export class ManagedProjectsDetailsComponent implements OnInit {
  @Input()
  public project: ProjectDTO;
  public projectCreator: UserDTO;
  public selectedProjectId: string;
  public behindTarget: number;

  constructor(
    private readonly router: Router,
    private readonly userService: UserService,
  ) { }

  ngOnInit(): void {
    this.userService
      .getSingleUser(this.project.projectCreator)
      .subscribe({
        next: (creator: UserDTO) => {
          this.projectCreator = creator;
        }
      });

    this.behindTarget = Math.abs(
      this.project.daysUntilCompletion - this.project.targetInDays
    );
  }

  public selectProject(id: string): void {
    this.selectedProjectId = id;
    this.router.navigate(['/project-details', id]);
  }


}
