import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { ManagedProjectsComponent } from './managed-projects.component';

describe('ManagedProjectsComponent', () => {
  let component: ManagedProjectsComponent;
  let fixture: ComponentFixture<ManagedProjectsComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ ManagedProjectsComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(ManagedProjectsComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
