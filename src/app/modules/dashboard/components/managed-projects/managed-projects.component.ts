import { Component, OnInit, Input } from '@angular/core';
import { ProjectDTO } from '../../../../models/projects/project.dto';

@Component({
  selector: 'app-managed-projects',
  templateUrl: './managed-projects.component.html',
  styleUrls: ['./managed-projects.component.css']
})
export class ManagedProjectsComponent implements OnInit {
  @Input()
  public userActiveProjects: ProjectDTO[];

  constructor(
  ) { }

  ngOnInit(): void {
  }
}
