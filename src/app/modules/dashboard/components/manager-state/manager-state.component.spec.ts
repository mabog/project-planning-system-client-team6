import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { ManagerStateComponent } from './manager-state.component';

describe('ManagerStateComponent', () => {
  let component: ManagerStateComponent;
  let fixture: ComponentFixture<ManagerStateComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ ManagerStateComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(ManagerStateComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
