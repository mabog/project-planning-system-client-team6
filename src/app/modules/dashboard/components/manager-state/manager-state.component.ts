import {
  Component,
  OnInit,
  Input,
  OnChanges,
  SimpleChanges
} from '@angular/core';
import { UserDTO } from '../../../../models/users/user.dto';
import { EmployeeDTO } from '../../../../models/employees/employee.dto';
import { ProjectDTO } from '../../../../models/projects/project.dto';
import { UserService } from '../../../../core/services/user.service';
import {
  ProjectStatus
} from '../../../../models/projects/enums/project-status.enum';
import { Router } from '@angular/router';

@Component({
  selector: 'app-manager-state',
  templateUrl: './manager-state.component.html',
  styleUrls: ['./manager-state.component.css']
})
export class ManagerStateComponent implements OnInit, OnChanges {
  @Input()
  public userInfo: UserDTO;
  @Input()
  public directManager: UserDTO;
  public greeting: string;
  public managerSubordinates: UserDTO[];
  public employeeSubordinates: EmployeeDTO[];
  public userActiveProjects: ProjectDTO[] = [];

  public isSubordinates: boolean;
  public isProjects: boolean;

  constructor(
    private readonly userService: UserService,
    private readonly router: Router,
  ) { }

  ngOnInit(): void {
    this.updateGreeting();
  }

  ngOnChanges(changes: SimpleChanges): void {
    if (this.userInfo && changes.userInfo) {
      this.displayProjects();
    }
  }

  public displayProjects(): void {
    this.isProjects = true;
    this.isSubordinates = false;

    if (this.userActiveProjects.length === 0) {
      this.userService.getProjectsForManager(this.userInfo.id)
        .subscribe({
          next: (projects: ProjectDTO[]) => {
            projects.map(project => {
              if (project.status === ProjectStatus.IN_PROGRESS) {
                this.userActiveProjects.push(project);
              }
            });
          },
        });
    }
  }


  public displaySubordinates(): void {
    this.isProjects = false;
    this.userActiveProjects = [];
    this.isSubordinates = true;
  }


  public displayProfile(): void {
    this.router.navigate(['/dashboard/profile']);
  }

  private updateGreeting(): void {
    if (new Date().getHours() < 9) {
      this.greeting = 'Good morning';
    } else if (new Date().getHours() > 9 && new Date().getHours() < 14) {
      this.greeting = 'Good day';
    } else if (new Date().getHours() > 14 && new Date().getHours() < 18) {
      this.greeting = 'Good afternoon';
    } else {
      this.greeting = 'Good evening';
    }
  }
}
