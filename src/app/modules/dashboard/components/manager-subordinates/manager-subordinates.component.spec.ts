import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { ManagerSubordinatesComponent } from './manager-subordinates.component';

describe('ManagerSubordinatesComponent', () => {
  let component: ManagerSubordinatesComponent;
  let fixture: ComponentFixture<ManagerSubordinatesComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ ManagerSubordinatesComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(ManagerSubordinatesComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
