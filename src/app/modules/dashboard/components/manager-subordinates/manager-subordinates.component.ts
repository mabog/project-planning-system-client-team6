import { Component, OnInit, Input } from '@angular/core';
import { UserDTO } from '../../../../models/users/user.dto';
import { EmployeeDTO } from '../../../../models/employees/employee.dto';
import { UserService } from '../../../../core/services/user.service';
import { Router } from '@angular/router';

@Component({
  selector: 'app-manager-subordinates',
  templateUrl: './manager-subordinates.component.html',
  styleUrls: ['./manager-subordinates.component.css']
})
export class ManagerSubordinatesComponent implements OnInit {
  @Input()
  public userInfo: UserDTO;
  public managerSubordinates: UserDTO[];
  public employeeSubordinates: EmployeeDTO[];

  constructor(
    private readonly userService: UserService,
    private readonly router: Router,
  ) { }

  ngOnInit(): void {
    this.userService.getMyInfo()
      .subscribe({
        next: (loggedUser: UserDTO) => {
          this.userInfo = loggedUser;
          this.getSubordinates();
        }
      });
  }

  private getSubordinates(): void {
    this.userService
      .getUserSubordinates(this.userInfo.id)
      .subscribe({
        next: (subordinates: UserDTO[]) => {
          this.managerSubordinates = subordinates;
        }
      });

    this.userService
      .getEmployeeSubordinates(this.userInfo.id)
      .subscribe({
        next: (subordinates: EmployeeDTO[]) => {
          this.employeeSubordinates = subordinates;
        }
      });
  }

  public selectUser(id: string): void {
    this.router.navigate(['/work-force/users', id]);
  }

  public selectEmployee(id: string): void {
    this.router.navigate(['/work-force/employees', id]);
  }
}
