import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';
import { BoardComponent } from './components/board.component';
import {
  ManagerProfileComponent
} from './components/manager-state/manager-profile/manager-profile.component';

const routes: Routes = [
  { path: '', component: BoardComponent },
  { path: 'profile', component: ManagerProfileComponent }
];

@NgModule({
  imports: [
    RouterModule.forChild(routes)
  ],
})
export class DashboardRoutingModule { }
