import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import {
  ManagedProjectsComponent
} from './components/managed-projects/managed-projects.component';
import {
  ManagerStateComponent
} from './components/manager-state/manager-state.component';
import { BoardComponent } from './components/board.component';
import { DashboardRoutingModule } from './dashboard-routing.module';
import { MaterialModule } from '../../material/material.module';
import { SharedModule } from '../../shared/shared.module';
import {
  ManagedProjectsDetailsComponent
} from './components/managed-projects/managed-projects-details/managed-projects-details.component';
import {
  ManagerSubordinatesComponent
} from './components/manager-subordinates/manager-subordinates.component';
import {
  ManagerProfileComponent
} from './components/manager-state/manager-profile/manager-profile.component';

@NgModule({
  declarations: [
    BoardComponent,
    ManagedProjectsComponent,
    ManagerStateComponent,
    ManagedProjectsDetailsComponent,
    ManagerSubordinatesComponent,
    ManagerProfileComponent,
  ],
  imports: [
    CommonModule,
    MaterialModule,
    SharedModule,
    DashboardRoutingModule,
  ],
})
export class DashboardModule { }
