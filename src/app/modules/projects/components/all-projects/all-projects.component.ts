
import { Component, OnInit, OnChanges, SimpleChanges } from '@angular/core';
import { ProjectService } from '../../../../core/services/project.service';
import { Router } from '@angular/router';

@Component({
  selector: 'app-all-projects',
  templateUrl: './all-projects.component.html',
  styleUrls: ['./all-projects.component.css']
})
export class AllProjectsComponent implements OnInit, OnChanges {

  public projects: any; // change

  constructor(
    private readonly server: ProjectService,
    private readonly router: Router,
  ) { }

  ngOnInit(): void {
    this.server.getAllProjects().subscribe(result => this.projects = result);
  }

  ngOnChanges(changes: SimpleChanges ): void {
    if (changes) {
      this.router.navigate(['/all-projects']);
    }
  }

}
