import { AssignmentReturnDTO } from '../../../../models/assignments/assignment-return.dto';
import { AssignmentCreateDTO } from '../../../../models/assignments/assignment-create.dto';
import { Employee } from '../../../../models/projects/employee';
import { ProjectReturnDTO } from '../../../../models/projects/project-return.dto';
import { Subscription, of, Observable } from 'rxjs';
import { RequirementService } from '../../../../core/services/requirement.service';
import { RequirementReturnDTO } from '../../../../models/projects/requirement-return.dto';
import { Component, OnInit, OnDestroy, Input } from '@angular/core';
import { FormGroup, FormControl, Validators } from '@angular/forms';
import { ProjectService } from '../../../../core/services/project.service';
import { EmployeeReturnDTO } from '../../../../models/projects/emploee-return.dto';
import { CompanySkills } from '../../../../models/projects/company-skills';
import { ProjectCreateDTO } from '../../../../models/projects/project-create.dto';
import { RequirementCreateDTO } from '../../../../models/projects/requirement-create.dto';
import { AssignmentService } from '../../../../core/services/assignment.service';
import { ActivatedRoute, Router } from '@angular/router';

@Component({
  selector: 'app-create-project',
  templateUrl: './create-project.component.html',
  styleUrls: ['./create-project.component.css']
})
export class CreateProjectComponent implements OnInit, OnDestroy {

@Input()
assignment: string;

  private projectSubscription: Subscription;
  private employeeSubscription: Subscription;
  private requirementSubscription: Subscription;
  private projectEditSubscription: Subscription;
  private assignmentSubsription: Subscription;
  private reloaded: boolean;

  public editMode;
  public employees: Employee[];
  public skills: string[] = Object.keys(CompanySkills);
  public createdProject$: ProjectReturnDTO ;
  public createdRequirements$: RequirementReturnDTO[] = [];
  public createdAssignments$: AssignmentReturnDTO[] = [];

  public createForm: FormGroup = new FormGroup( {
    name: new FormControl('', [Validators.required, Validators.minLength(5)]),
    description: new FormControl('', [Validators.required, Validators.minLength(5)]),
    targetInDays: new FormControl('', [Validators.required, Validators.minLength(1)]),
    managerWorkInputPerDay: new FormControl('', [Validators.required, Validators.minLength(1)]),
  });

  public skillForm: FormGroup = new FormGroup ({
    skill:  new FormControl(),
    hoursSkill: new FormControl('', [Validators.required, ]),
  });

  public employeeForm: FormGroup = new FormGroup ({
    assignment: new FormControl(),
    employee: new FormControl(),
    hoursEmployee: new FormControl('', [Validators.required, Validators.minLength(1)])
  });


  constructor(
    private readonly projectService: ProjectService,
    private readonly requirementService: RequirementService,
    private readonly assignementService: AssignmentService,
    private readonly activatedRoute: ActivatedRoute,
    private readonly router: Router,
  ) { }
    // get project data
    private getProjectInputData(): ProjectCreateDTO {
      const newProject: ProjectCreateDTO = new ProjectCreateDTO();
      newProject.name = this.createForm.value.name;
      newProject.description = this.createForm.value.description;
      newProject.targetInDays = this.createForm.value.targetInDays;
      newProject.managerWorkInputPerDay = this.createForm.value.managerWorkInputPerDay;
      return newProject;
    }

    // create project
    public createProject(): void{
      this.createdProject$ = new ProjectReturnDTO();
      this.projectSubscription = this.projectService.createProject(
          this.getProjectInputData()).subscribe(project => {
            this.createdProject$ = project;
            this.createForm.reset();
      });
    }

    public updateProject() {
      this.projectSubscription = this.projectService.updateProject(this.createdProject$.id,
        this.getProjectInputData()).subscribe(project => {
          this.createdProject$ = project;
          this.createForm.reset();
          this.ngOnInit();
    });

    }
    // get req & hours
    private getRequirementInputData(): RequirementCreateDTO {
      const newRequirement: RequirementCreateDTO = new RequirementCreateDTO();
      newRequirement.reqSkill = this.skillForm.value.skill;
      newRequirement.totalReqWorkTime = this.skillForm.value.hoursSkill;
      return newRequirement;
    }

    // add req
    public addRequirement(): void{
      this.requirementSubscription = this.requirementService.createRequirement(
        this.createdProject$.id, this.getRequirementInputData())
          .subscribe(result => {
            this.createdRequirements$.push(result);
            this.skillForm.reset();
            this.ngOnInit();
          });
    }

    // get employee name & hours
    private getAssignmentInputData(): AssignmentCreateDTO {
      const newAssignment: AssignmentCreateDTO = new AssignmentCreateDTO();
      newAssignment.reqId = this.employeeForm.value.assignment;
      newAssignment.employeeWorkInputPerDay = this.employeeForm.value.hoursEmployee;
      return newAssignment;
    }

  // add assignment
  public addAssignment(): void{
    const employeeID: string = this.employeeForm.value.employee.id;
    this.employeeSubscription =
      this.assignementService.assignWork(employeeID, this.getAssignmentInputData())
      .subscribe(result => {
        this.createdAssignments$.push(result);
        console.log(this.createdAssignments$);
        this.employeeForm.reset();
        this.ngOnInit();
      });
    }

ngOnInit(): void {
  // load project for edit
  if (this.router.url !== '/create-project'){
    this.editMode = true;
    const projectID: string = this.activatedRoute.snapshot.params[`id`];
    this.projectEditSubscription = this.projectService.getProjectByID(projectID)
  .subscribe(result => {
    this.createdProject$ = result;


    // get reqs
    this.requirementSubscription = this.requirementService.getRequirements(
      this.createdProject$.id).subscribe(resultReq => {
        this.createdRequirements$ = resultReq;

        // get assignments for every req
        for (const req of this.createdRequirements$) {
          this.assignmentSubsription = this.assignementService.getAssignments(req.id).subscribe(assnmts =>
            this.createdAssignments$ = assnmts);
        }

        if (!this.reloaded) {
          this.reloaded = true;
          // this.ngOnInit();
        }
      });
    });
  }

  // populater employee names dropdown
  this.employeeSubscription = this.projectService.getAllEmployyes().subscribe(
    result => {
      const emloyeesDTOs: EmployeeReturnDTO[] = result;

      this.employees = emloyeesDTOs.map((employee) => ({
        name: `${employee.firstName} ${employee.lastName}`,
        id:  `${employee.id}`
      }));
    }
  );
}

ngOnDestroy(): void {
  if (this.projectSubscription){
    this.projectSubscription.unsubscribe();
  }

  if (this.requirementSubscription){
    this.requirementSubscription.unsubscribe();
  }

  if (this.employeeSubscription){
    this.employeeSubscription.unsubscribe();
  }

  if (this.assignmentSubsription){
    this.assignmentSubsription.unsubscribe();
  }

}

}
