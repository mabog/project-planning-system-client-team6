import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { MyManagedProjectsComponent } from './my-managed-projects.component';

describe('MyManagedProjectsComponent', () => {
  let component: MyManagedProjectsComponent;
  let fixture: ComponentFixture<MyManagedProjectsComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ MyManagedProjectsComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(MyManagedProjectsComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
