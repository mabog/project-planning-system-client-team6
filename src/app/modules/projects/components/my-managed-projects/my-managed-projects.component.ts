import { Component, OnInit, OnDestroy } from '@angular/core';
import { Subscription } from 'rxjs';
import { ProjectReturnDTO } from '../../../../models/projects/project-return.dto';
import { AuthService } from '../../../../core/services/auth.service';

@Component({
  selector: 'app-my-managed-projects',
  templateUrl: './my-managed-projects.component.html',
  styleUrls: ['./my-managed-projects.component.css']
})
export class MyManagedProjectsComponent implements OnInit, OnDestroy {

  private loggedUserSubscription: Subscription;
  public loggedUser: any; // change
  public projects: ProjectReturnDTO[];

  constructor(
    private readonly auth: AuthService,

  ) { }

  ngOnInit(): void {
    this.loggedUserSubscription = this.auth.loggedInUser$.subscribe((loggedUser) => {
      this.loggedUser = loggedUser;

      // need user id from user email
      // this.server.getProjectsByManagerID(this.loggedUser).
      //   subscribe(result => this.projects = result);
    });

  }

  ngOnDestroy(): void {
    this.loggedUserSubscription.unsubscribe();
  }

}
