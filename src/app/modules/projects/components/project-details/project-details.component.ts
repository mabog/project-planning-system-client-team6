import { EmployeeService } from './../../../../core/services/employee.service';
import { AssignmentReturnDTO } from './../../../../models/assignments/assignment-return.dto';
import { UserService } from './../../../../core/services/user.service';
import { RequirementReturnDTO } from '../../../../models/projects/requirement-return.dto';
import { ProjectService } from '../../../../core/services/project.service';
import { ActivatedRoute, Router } from '@angular/router';
import { Component, OnInit, OnDestroy} from '@angular/core';
import { ProjectReturnDTO } from '../../../../models/projects/project-return.dto';
import { Subscription } from 'rxjs/internal/Subscription';
import { RequirementService } from '../../../../core/services/requirement.service';
import { AssignmentService } from '../../../../core/services/assignment.service';

@Component({
  selector: 'app-project-details',
  templateUrl: './project-details.component.html',
  styleUrls: ['./project-details.component.css']
})
export class ProjectDetailsComponent implements OnInit, OnDestroy {

  private projectSubscription: Subscription;
  private deleteSubscription: Subscription;
  private requirementSubscription: Subscription;
  private assignmentSubsription: Subscription;

  public project$: ProjectReturnDTO;
  public reqs$: RequirementReturnDTO[];
  public deleteMessage$: {};
  public managerName$: string;
  public assignments$: AssignmentReturnDTO[];
  public employeeNames: string[] = [];

  constructor(
    private readonly activatedRoute: ActivatedRoute,
    private readonly projectService: ProjectService,
    private readonly router: Router,
    public readonly userService: UserService,
    private readonly requirementService: RequirementService,
    private readonly assignmentService: AssignmentService,
    private readonly employeeService: EmployeeService
  ) { }

// delete project
  public deleteProject() {
    this.deleteSubscription = this.projectService.deleteProject(
      this.project$.id
      ).subscribe(result => this.deleteMessage$ = result);

    this.router.navigate(['/all-projects']);
  }


  ngOnInit() {
    // get project
    const projectID: string = this.activatedRoute.snapshot.params[`id`];
    this.projectSubscription = this.projectService.getProjectByID(projectID).subscribe(
      result => {
        this.project$ = result;
      }
    );

    // get manager name
    this.userService.getMyInfo().subscribe(result =>
      this.managerName$ = result.firstName + ' ' + result.lastName);

    // get reqs
    this.requirementSubscription = this.requirementService.getRequirements(
      projectID).subscribe(resultReq => {
        this.reqs$ = resultReq;
        // get assignments for every req
        for (const req of this.reqs$) {
          this.assignmentSubsription = this.assignmentService
          .getAssignments(req.id).subscribe(assnmts => {
              this.assignments$ = assnmts;
              for (const assignment of this.assignments$) {

                // get employees names
                this.employeeService.getEmployee(assignment.assigneeId).subscribe(employee =>
                this.employeeNames.push(employee.firstName + ' ' + employee.lastName));
              }
          });
        }
      }
    );

  }

  ngOnDestroy(): void {
    if (this.projectSubscription) {
      this.projectSubscription.unsubscribe();
    }

    if (this.requirementSubscription) {
      this.requirementSubscription.unsubscribe();
    }

    if (this.assignmentSubsription) {
      this.assignmentSubsription.unsubscribe();
    }

    if (this.deleteSubscription) {
      this.deleteSubscription.unsubscribe();
    }
  }

}
