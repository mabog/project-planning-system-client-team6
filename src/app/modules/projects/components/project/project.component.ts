import { Component, OnInit, Input, Output, EventEmitter } from '@angular/core';

@Component({
  selector: 'app-project',
  templateUrl: './project.component.html',
  styleUrls: ['./project.component.css']
})
export class ProjectComponent implements OnInit {

  // needs types and all fields
  @Input()
  project: {id, name, description, status, targetInDays};

  constructor() { }

  ngOnInit(): void {
  }

}
