import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { ProjectsComponent } from './components/projects.component';
import { AllProjectsComponent } from './components/all-projects/all-projects.component';
import { MyManagedProjectsComponent } from './components/my-managed-projects/my-managed-projects.component';
import { CreateProjectComponent } from './components/create-project/create-project.component';
import { ProjectDetailsComponent } from './components/project-details/project-details.component';
import { EditProjectComponent } from './components/edit-project/edit-project.component';


const routes: Routes = [
  { path: 'all-projects', component: AllProjectsComponent },
  { path: 'my-managed-projects', component: MyManagedProjectsComponent },
  { path: 'create-project', component: CreateProjectComponent },
  { path: 'project-details/:id', component: ProjectDetailsComponent },
  ];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class ProjectsRoutingModule { }
