import { ProjectsComponent } from './components/projects.component';
import { NgModule } from '@angular/core';
import { AllProjectsComponent } from './components/all-projects/all-projects.component';
import { MyManagedProjectsComponent } from './components/my-managed-projects/my-managed-projects.component';
import { ProjectComponent } from './components/project/project.component';
import { ProjectDetailsComponent } from './components/project-details/project-details.component';
import { CreateProjectComponent } from './components/create-project/create-project.component';
import { CommonModule } from '@angular/common';
import { ProjectsRoutingModule } from './projects-routing.module';



@NgModule({
  declarations: [
    ProjectsComponent,
    AllProjectsComponent,
    MyManagedProjectsComponent,
    ProjectComponent,
    ProjectDetailsComponent,
    CreateProjectComponent,

  ],
  imports: [
    CommonModule,
    ProjectsRoutingModule
  ]
})
export class ProjectsModule { }
