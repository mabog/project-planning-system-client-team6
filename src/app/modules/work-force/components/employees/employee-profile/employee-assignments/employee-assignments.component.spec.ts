import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { EmployeeAssignmentsComponent } from './employee-assignments.component';

describe('EmployeeAssignmentsComponent', () => {
  let component: EmployeeAssignmentsComponent;
  let fixture: ComponentFixture<EmployeeAssignmentsComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ EmployeeAssignmentsComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(EmployeeAssignmentsComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
