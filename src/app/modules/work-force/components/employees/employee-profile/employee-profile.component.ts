import { Component, OnInit } from '@angular/core';
import { ActivatedRoute, Router } from '@angular/router';
import { EmployeeDTO } from '../../../../../models/employees/employee.dto';
import { EmployeeService } from '../../../../../core/services/employee.service';

@Component({
  selector: 'app-employee-profile',
  templateUrl: './employee-profile.component.html',
  styleUrls: ['./employee-profile.component.css']
})
export class EmployeeProfileComponent implements OnInit {
  public employeeId: string;
  public employeeInfo: EmployeeDTO;

  constructor(
    private readonly route: ActivatedRoute,
    private readonly employeeService: EmployeeService,
  ) { }

  ngOnInit(): void {
    this.employeeId = this.route.snapshot.params.id;

    this.employeeService.getSingleEmployee(this.employeeId)
      .subscribe({
        next: (employee: EmployeeDTO) => {
          this.employeeInfo = employee;
        }
      });
  }

}
