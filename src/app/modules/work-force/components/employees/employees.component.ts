import { Component, OnInit } from '@angular/core';
import { EmployeeDTO } from '../../../../models/employees/employee.dto';
import { EmployeeService } from '../../../../core/services/employee.service';
import { Router } from '@angular/router';

@Component({
  selector: 'app-employees',
  templateUrl: './employees.component.html',
  styleUrls: ['./employees.component.css']
})
export class EmployeesComponent implements OnInit {
  public employees: EmployeeDTO[];

  constructor(
    private readonly router: Router,
    private readonly employeeService: EmployeeService,
  ) { }

  ngOnInit(): void {
    this.employeeService
      .getAllEmployees()
      .subscribe({
        next: (data: EmployeeDTO[]) => {
          this.employees = data;
        }
      });
  }

  public selectEmployee(id: string): void {
    this.router.navigate(['/work-force/employees', id]);
  }
}
