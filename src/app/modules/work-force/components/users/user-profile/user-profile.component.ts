import { Component, OnInit, OnChanges, SimpleChanges } from '@angular/core';
import { ActivatedRoute } from '@angular/router';
import { UserService } from '../../../../../core/services/user.service';
import { UserDTO } from '../../../../../models/users/user.dto';
import { EmployeeDTO } from '../../../../../models/employees/employee.dto';
import { ProjectDTO } from '../../../../../models/projects/project.dto';
import {
  ProjectStatus
} from '../../../../../models/projects/enums/project-status.enum';

@Component({
  selector: 'app-user-profile',
  templateUrl: './user-profile.component.html',
  styleUrls: ['./user-profile.component.css']
})
export class UserProfileComponent implements OnInit, OnChanges {
  public userId: string;
  public userInfo: UserDTO;
  public managerSubordinates: UserDTO[];
  public employeeSubordinates: EmployeeDTO[];
  public userActiveProjects: ProjectDTO[] = [];

  public isSubordinates: boolean;
  public isProjects: boolean;


  constructor(
    private readonly route: ActivatedRoute,
    private readonly userService: UserService,
  ) { }

  ngOnInit(): void {
    this.userId = this.route.snapshot.params.id;

    this.userService.getSingleUser(this.userId)
      .subscribe({
        next: (user: UserDTO) => {
          this.userInfo = user;
        }
      });
  }

  ngOnChanges(changes: SimpleChanges): void {
    if (this.userInfo && changes.userInfo) {
      this.displayProjects();
    }
  }

  public displayProjects(): void {
    this.isProjects = true;
    this.isSubordinates = false;

    if (this.userActiveProjects.length === 0) {
      this.userService.getProjectsForManager(this.userInfo.id)
        .subscribe({
          next: (projects: ProjectDTO[]) => {
            projects.map(project => {
              if (project.status === ProjectStatus.IN_PROGRESS) {
                this.userActiveProjects.push(project);
              }
            });
          },
        });
    }
  }

  public displaySubordinates(): void {
    this.isProjects = false;
    this.userActiveProjects = [];
    this.isSubordinates = true;
  }

}
