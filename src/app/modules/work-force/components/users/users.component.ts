import { Component, OnInit } from '@angular/core';
import { UserDTO } from '../../../../models/users/user.dto';
import { UserService } from '../../../../core/services/user.service';
import { Router } from '@angular/router';

@Component({
  selector: 'app-users',
  templateUrl: './users.component.html',
  styleUrls: ['./users.component.css']
})
export class UsersComponent implements OnInit {
  public users: UserDTO[];

  constructor(
    private readonly router: Router,
    private readonly userService: UserService,
  ) { }

  ngOnInit(): void {
    this.userService
      .getAllUsers()
      .subscribe({
        next: (data: UserDTO[]) => {
          this.users = data;
        }
      });
  }

  public selectUser(id: string): void {
    this.router.navigate(['/work-force/users', id]);
  }

}
