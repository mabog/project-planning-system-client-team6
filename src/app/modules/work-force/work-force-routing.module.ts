import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';
import { WorkForceComponent } from './components/work-force.component';
import {
  EmployeeProfileComponent
} from './components/employees/employee-profile/employee-profile.component';
import {
  UserProfileComponent
} from './components/users/user-profile/user-profile.component';

const routes: Routes = [
  { path: '', component: WorkForceComponent },
  { path: 'employees/:id', component: EmployeeProfileComponent },
  { path: 'users/:id', component: UserProfileComponent },
];

@NgModule({
  declarations: [],
  imports: [
    RouterModule.forChild(routes)
  ]
})
export class WorkForceRoutingModule { }
