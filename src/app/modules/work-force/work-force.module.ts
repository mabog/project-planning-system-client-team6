import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { MaterialModule } from '../../material/material.module';
import { SharedModule } from '../../shared/shared.module';
import { WorkForceRoutingModule } from './work-force-routing.module';
import { WorkForceComponent } from './components/work-force.component';
import {
  EmployeeProfileComponent
} from './components/employees/employee-profile/employee-profile.component';
import {
  EmployeeUpdateComponent
} from './components/employees/employee-update/employee-update.component';
import {
  UserProfileComponent
} from './components/users/user-profile/user-profile.component';
import {
  UserUpdateComponent
} from './components/users/user-update/user-update.component';
import { UsersComponent } from './components/users/users.component';
import { EmployeesComponent } from './components/employees/employees.component';
import {
  EmployeeAssignmentsComponent
} from './components/employees/employee-profile/employee-assignments/employee-assignments.component';

@NgModule({
  declarations: [
    WorkForceComponent,
    EmployeeProfileComponent,
    EmployeeUpdateComponent,
    UsersComponent,
    UserProfileComponent,
    UserUpdateComponent,
    EmployeesComponent,
    EmployeeAssignmentsComponent,
  ],
  imports: [
    CommonModule,
    MaterialModule,
    SharedModule,
    WorkForceRoutingModule,
  ]
})
export class WorkForceModule { }
