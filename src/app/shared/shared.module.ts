import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { MaterialModule } from '../material/material.module';
import { NgxSpinnerModule } from 'ngx-spinner';
import { RouterModule } from '@angular/router';

@NgModule({
  declarations: [],
  imports: [
    CommonModule,
    MaterialModule,
    NgxSpinnerModule,
    RouterModule,
  ],
  exports: [
    NgxSpinnerModule
  ],
})
export class SharedModule { }
